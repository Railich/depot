class StoreController < ApplicationController
  def index
    # создадим переменную экземпляра продуктов, отсортируем по заголовку
    @products = Product.order(:title)
  end
end
