class Product < ActiveRecord::Base
  validates :title, :description, :image_url, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }
  validates :title, uniqueness: true
  validates :title, length: { minimum: 10, message: 'Длинна заголовка должна быть не менее 10 символов' }
  validates :image_url, allow_blank: true, format: {
      with: %r{\.(gif|jpg|png)\Z}i,
      message: 'URL должен указывать на изображение формата gif, jpg, png.'
  }
  
  #для кэширования, метод получает дату последнего изменения
  def self.latest
    Product.order(:updated_at).last
  end
end
